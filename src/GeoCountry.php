<?php

namespace LikeTheArms\Geoname;

use LikeTheArms\Geoname\Geoname;

class GeoCountry extends Geoname
{
    protected $table = 'countries';

    public function cities()
    {
        return $this->hasMany('\LikeTheArms\Geoname\GeoCity', 'iso', 'iso');
    }
}
