<?php

namespace LikeTheArms\Geoname;

use Illuminate\Support\ServiceProvider;

class GeonameServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('geoname', function($app){
            return new Geoname;
        });
    }

    public function boot()
    {
        //
    }
}
