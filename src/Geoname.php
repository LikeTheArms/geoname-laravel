<?php

namespace LikeTheArms\Geoname;

use Illuminate\Database\Eloquent\Model;

class Geoname extends Model
{
    protected $connection = 'geonames';
}
