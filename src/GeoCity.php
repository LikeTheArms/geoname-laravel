<?php

namespace LikeTheArms\Geoname;

use LikeTheArms\Geoname\Geoname;

class GeoCity extends Geoname
{
    protected $table = 'cities';

    public function country()
    {
        return $this->belongsTo('\LikeTheArms\Geoname\GeoCity', 'iso', 'iso');
    }
}
